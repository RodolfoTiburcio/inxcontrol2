from django.urls import path, include
from .views import (UserCreateView, UserListView, UserDetailView, UserUpdateView,
                    SupplierListView, SupplierDetailView,SupplierCreateView, SupplierUpdateView,
                    ClientListView, ClientDetailView,ClientCreateView, ClientUpdateView, 
                    ClientContactUpdateView, ClientContactCreateView,  ClientContactDetailView, ClientContactListView,
                    SupplierContactUpdateView, SupplierContactCreateView,  SupplierContactDetailView, SupplierContactListView,
                    WorkerCreateView, WorkerTimeCreateView, WorkerTimeUpdateView, WorkerTimeWeekArchiveView,
                    WorkerTimeLastWeek, WorkerCostCreateView, WorkerCostUpdateView)

from django.contrib.auth.views import LoginView

app_name = 'profiles'

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('usercreate', UserCreateView.as_view(), name='user_create'),
    path('userlist', UserListView.as_view(), name='user_list'),
    path('login', LoginView.as_view(), name='user_login'),
    path('detail/<int:pk>', UserDetailView.as_view(), name = 'user_detail'),
    path('userupdate/<int:pk>', UserUpdateView.as_view(), name='user_update'),
    path('workercreate', WorkerCreateView.as_view(), name = 'worker_create'),
    path('workertimecreate', WorkerTimeCreateView.as_view(), name='workertime_create'),
    path('workertimeupdate/<int:pk>', WorkerTimeUpdateView.as_view(), name='workertime_update'),
    path('workertime/<int:year>/week/<int:week>', WorkerTimeWeekArchiveView.as_view(), name='workertime_week'),
    path('workertime/last', WorkerTimeLastWeek.as_view(), name='workertime_lastweek'),
    path('workercostcreate/<int:pk>', WorkerCostCreateView.as_view(), name='workercost_create'),
    path('workercostupdate/<int:pk>', WorkerCostUpdateView.as_view(), name='workercost_update'),
    path('clientcreate', ClientCreateView.as_view(), name='client_create'),
    path('clientlist', ClientListView.as_view(), name = 'client_list'),
    path('clientdetail/<int:pk>', ClientDetailView.as_view(), name='client_detail'),
    path('clientupdate/<int:pk>', ClientUpdateView.as_view(), name='client_update'),
    path('suppliercreate', SupplierCreateView.as_view(), name='supplier_create'),
    path('supplierlist', SupplierListView.as_view(), name='supplier_list'),
    path('supplierdetail/<int:pk>', SupplierDetailView.as_view(), name='supplier_detail'),
    path('supplierupdate/<int:pk>', SupplierUpdateView.as_view(), name='supplier_update'),
    path('clientcontactcreate', ClientContactCreateView.as_view(), name='clientcontact_create'),
    path('clientcontactlist', ClientContactListView.as_view(), name = 'clientcontact_list'),
    path('clientcontactdetail/<int:pk>', ClientContactDetailView.as_view(), name='clientcontact_detail'),
    path('clientcontactupdate/<int:pk>', ClientContactUpdateView.as_view(), name='clientcontact_update'),
    path('suppliercontactcreate', SupplierContactCreateView.as_view(), name='suppliercontact_create'),
    path('suppliercontactlist', SupplierContactListView.as_view(), name = 'suppliercontact_list'),
    path('suppliercontactdetail/<int:pk>', SupplierContactDetailView.as_view(), name='suppliercontact_detail'),
    path('suppliercontactupdate/<int:pk>', SupplierContactUpdateView.as_view(), name='suppliercontact_update'),
]
