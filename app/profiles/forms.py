from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ModelMultipleChoiceField

from .models import (SupplierContact, ClientContact, Client,
                     Supplier, WorkersTime, WorkerCost)

class UserModelChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.get_full_name()

class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name']
        labels = {
            'username':'Nombre de usuario',
            'first_name':'Nombre',
            'last_name':'Apellidos',
        }

class UserUpdateForm(UserChangeForm):
    class Meta:
        model = User
        fields = '__all__'
        labels = {
            'username':'Nombre de usuario',
            'first_name':'Nombre',
            'last_name':'Apellidos',
            'password1':'Contrasena',
            'password2':'Confirme contraseña',
        }

class WorkerCreateForm(forms.Form):
    first_name = forms.CharField(
        label = 'Nombre',
        max_length= 63,
        help_text = "Nombre del trabajador"
    )
    last_name = forms.CharField(
        label='Apellidos',
        max_length= 64,
        help_text='Apellidos del trabajador',
    )

class ClientContactCreateForm(forms.ModelForm):
    class Meta:
        model = ClientContact
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'phone':'Telefono',
            'notes':'Notas',
            'client':'Cliente',
        }

class ClientContactUpdateForm(forms.ModelForm):
    class Meta:
        model = ClientContact
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'phone':'Telefono',
            'notes':'Notas',
            'client':'Cliente',
        }

class SupplierContactCreateForm(forms.ModelForm):
    class Meta:
        model = SupplierContact
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'phone':'Telefono',
            'notes':'Notas',
            'supplier':'Proveedor',
        }

class SupplierContactUpdateForm(forms.ModelForm):
    class Meta:
        model = SupplierContact
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'phone':'Telefono',
            'notes':'Notas',
            'supplier':'Proveedor',
        }

class ClientCreateForm(forms.ModelForm):
    class Meta:
        model = Client
        exclude = ['status']
        labels = {
            'name':'Nombre',
            'score':'Calificación',
        }

class ClientUpdateForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'status':'Estado',
            'score':'Calificación',
        }

class SupplierCreateForm(forms.ModelForm):
    class Meta:
        model = Supplier
        fields = '__all__'
        labels = {
            'name':'Nombre',
        }

class SupplierUpdateForm(forms.ModelForm):
    class Meta:
        model = Supplier
        fields = '__all__'
        labels = {
            'name':'Nombre',
        }

class WorkerTimeCreateForm(forms.ModelForm):
    worker = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
        widget = forms.CheckboxSelectMultiple(),
        label = 'Trabajadores',
    )
    class Meta:
        model = WorkersTime
        fields = '__all__'
        widgets = {
            'workdate':forms.DateInput(attrs={
                'class':'datepicker form-control',
                'type':'date',
            }),
        }
        labels = {
            'workdate':'Fecha',
            'client':'Cliente',
        }

class WorkerTimeUpdateForm(forms.ModelForm):
    worker = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
        widget = forms.CheckboxSelectMultiple(),
        label = 'Trabajadores',
    )
    class Meta:
        model = WorkersTime
        fields = '__all__'
        widgets = {
            'workdate':forms.DateInput(attrs={
                'class':'datepicker form-control',
                'type':'date',
            }),
        }
        labels = {
            'workdate':'Fecha',
            'client':'Cliente',
        }

class WorkerCostCreateForm(forms.ModelForm):
    class Meta:
        model = WorkerCost
        fields = '__all__'
        widgets = {
            'worker':forms.HiddenInput()
        }