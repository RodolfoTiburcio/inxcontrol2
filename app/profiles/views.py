from typing import Any, Dict
from django.db.models.query import QuerySet
from django.db.models import Sum
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, WeekArchiveView, RedirectView
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.contrib.auth.models import User, Group
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Client, SupplierContact, ClientContact, Supplier, WorkersTime, WorkerCost
from finances.models import Movement
from projects.models import Report, Activity

from .forms import (UserRegistrationForm, UserUpdateForm,
                    ClientContactCreateForm, SupplierContactCreateForm, ClientCreateForm, SupplierCreateForm,
                    ClientContactUpdateForm, SupplierContactUpdateForm, ClientUpdateForm, SupplierUpdateForm,
                    WorkerCreateForm, WorkerTimeCreateForm, WorkerTimeUpdateForm, WorkerCostCreateForm)

import datetime
"""
Clases para la administración de usuarios
"""
class UserCreateView(LoginRequiredMixin, CreateView):
    model = User
    form_class = UserRegistrationForm
    success_url = reverse_lazy('profiles:user_list')

class UserListView(LoginRequiredMixin, ListView):
    model = User
    context_object_name = 'users_list'

    def get_queryset(self) -> QuerySet[Any]:
        return User.objects.filter(is_active=True).order_by('pk')

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        # for each_user in context['users_list']:
        #     print(each_user.movement_set.all().filter(creation_date__month = 9))
        return context

class UserListBalanceView(LoginRequiredMixin, ListView):
    model = User
    context_object_name = 'user_list'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        return context

class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    context_object_name = 'user'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        # print(context['user'].movement_set.all())
        return context

class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    form_class = UserUpdateForm
    success_url = reverse_lazy('profiles:user_list')

class WorkerCreateView(LoginRequiredMixin, FormView):
    form_class = WorkerCreateForm
    template_name = 'profiles/worker_form.html'
    success_url = reverse_lazy('profiles:user_list')

    def form_valid(self, form):
        # print(self.request.POST['first_name'])
        last_id = User.objects.all().order_by('-pk')[0]
        new_username = 'user_'+ str(last_id.pk+1)
        newUser = User.objects.create(
            username=new_username,
            first_name = self.request.POST['first_name'],
            last_name=self.request.POST['last_name'],
        )
        
        workergroup = Group.objects.get(name='Trabajador')
        workergroup.user_set.add(newUser)
        return super().form_valid(form)

"""
Clases para la administración de contactos de proveedor
"""
class SupplierContactCreateView(LoginRequiredMixin, CreateView):
    model = SupplierContact
    form_class = SupplierContactCreateForm
    success_url = reverse_lazy('profiles:suppliercontact_list')

class SupplierContactDetailView(LoginRequiredMixin, DetailView):
    model = SupplierContact
    context_object_name = 'supplierontact'

class SupplierContactListView(LoginRequiredMixin, ListView):
    model = SupplierContact
    context_object_name = 'suppliercontact_list'
    login_url=reverse_lazy('profiles:suppliercontact_list')

class SupplierContactUpdateView(LoginRequiredMixin, UpdateView):
    model = SupplierContact
    form_class = SupplierContactUpdateForm
    success_url = reverse_lazy('profiles:suppliercontact_list')

"""
Clases para la administración de contactos de clientes
"""
class ClientContactCreateView(LoginRequiredMixin, CreateView):
    model = ClientContact
    form_class = ClientContactCreateForm
    success_url = reverse_lazy('profiles:clientcontact_list')

    def get_form_kwargs(self):
        kwargs = super(ClientContactCreateView, self).get_form_kwargs()
        kwargs['initial'] = {
            'client':self.request.GET.get('client'),
        }
        return kwargs

class ClientContactListView(LoginRequiredMixin, ListView):
    model = ClientContact
    context_object_name = 'clientcontact_list'
    login_url=reverse_lazy('profiles:clientcontact_list')

class ClientContactDetailView(LoginRequiredMixin, DetailView):
    model = ClientContact
    context_object_name = 'clientcontact'

class ClientContactUpdateView(LoginRequiredMixin, UpdateView):
    model = ClientContact
    form_class = ClientContactUpdateForm
    success_url = reverse_lazy('profiles:clientcontact_list')

"""
Clases para la administración de proveedores
"""
class SupplierCreateView(LoginRequiredMixin, CreateView):
    model = Supplier
    form_class = SupplierCreateForm
    success_url = reverse_lazy('profiles:supplier_list')

class SupplierListView(LoginRequiredMixin, ListView):
    model = Supplier
    context_object_name = 'supplier_list'

class SupplierDetailView(LoginRequiredMixin, DetailView):
    model = Supplier
    context_object_name = 'supplier'

class SupplierUpdateView(LoginRequiredMixin, UpdateView):
    model = Supplier
    form_class = SupplierUpdateForm
    success_url = reverse_lazy('profiles:supplier_list')

"""
Clases para la administración de clientes
"""
class ClientCreateView(LoginRequiredMixin, CreateView):
    model = Client
    form_class = ClientCreateForm
    success_url = reverse_lazy('profiles:client_list')

class ClientListView(LoginRequiredMixin, ListView):
    model = Client
    context_object_name = 'client_list'
    ordering = 'name'

class ClientDetailView(LoginRequiredMixin, DetailView):
    model = Client
    context_object_name = 'client'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['balance'] = Movement.objects.filter(
            responsable = User.objects.get(pk=self.request.user.pk)
        )
        return context

class ClientUpdateView(LoginRequiredMixin, UpdateView):
    model = Client
    form_class = ClientUpdateForm
    success_url = reverse_lazy('profiles:client_list')

"""
Clases para la administración de los timepos de trabajo
"""
class WorkerTimeCreateView(LoginRequiredMixin, CreateView):
    model = WorkersTime
    form_class = WorkerTimeCreateForm
    success_url = reverse_lazy('profiles:workertime_lastweek')

class WorkerTimeUpdateView(LoginRequiredMixin, UpdateView):
    model = WorkersTime
    form_class = WorkerTimeUpdateForm
    success_url = reverse_lazy('profiles:workertime_lastweek')

class WorkerTimeWeekArchiveView(LoginRequiredMixin, WeekArchiveView):
    model = Report
    date_field = 'date'
    week_format = '%W'
    context_object_name = 'workerstime'
    allow_future = True
    allow_empty = True
    ordering = 'date'
    template_name = './profiles/workerstime_archive_week.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        queryset = self.get_dated_items()
        context['week_dates'] = [x[0] for x in queryset[1].values_list('date').distinct()]
        act_ids = set([x[0] for x in queryset[1].values_list('activity').distinct()])
        act = [Activity.objects.get(pk=a) for a in act_ids if a is not None]
        act_proj = []
        for a in act:
            if a.project:
                act_proj.append({
                    'act':a.pk,
                    'proj':a.project.pk
                })
            else:
                act_proj.append({
                    'act':a.pk,
                    'proj':0,
                })
        sorted_act = sorted(act_proj, key=lambda d: d['proj'])
        activities = [a['act'] for a in sorted_act]
        calendar_table = []
        for activity in activities:
            workday = []
            if(activity):
                workday.append(Activity.objects.get(pk=activity))
                for day in context['week_dates']:
                    workday.append(queryset[1].filter(date=day, activity=activity)
                    )
                calendar_table.append(workday)
        context['calendar_table'] = calendar_table
        return context

class WorkerTimeLastWeek(LoginRequiredMixin, RedirectView):
    year, week_num, day_of_week = datetime.date.today().isocalendar()
    url = reverse_lazy('profiles:workertime_week', kwargs={'year':year, 'week':week_num})


"""
Clases para la gestión de los costos de los trabajadores
"""

class WorkerCostCreateView(LoginRequiredMixin, CreateView):
    model = WorkerCost
    form_class = WorkerCostCreateForm

    def get_success_url(self) -> str:
        return reverse_lazy('profiles:user_detail', kwargs={'pk':self.kwargs['pk']})

    def get_form_kwargs(self):
        kwargs = super(WorkerCostCreateView, self).get_form_kwargs()
        kwargs['initial'] = {
            'worker':self.kwargs['pk']
        }
        return kwargs
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['user'] = User.objects.get(pk=self.kwargs['pk'])
        return context

class WorkerCostUpdateView(LoginRequiredMixin, UpdateView):
    model = WorkerCost
    form_class = WorkerCostCreateForm

    def get_success_url(self) -> str:
        return reverse_lazy('profiles:user_detail', kwargs={'pk':self.kwargs['pk']})