from django.urls import path
from finances.views import (MovementCreateView, MovementUpdateView, MovementDeleteView,
                            MovementMontArchiveView,MovementLastMonth ,MovementAdminListView,
                            WorkersTimeListView, AttendanceReport, AttendanceReportFormClass,
                            AttendaceFullReportClass, UserAccountingTemplateView,
                            ProjectAttendanceReport)

app_name = 'finances'

urlpatterns = [
    path('', MovementCreateView.as_view(), name = 'finances_home'),
    path('currentmonth',MovementLastMonth.as_view(), name='finances_lastmonth'),
    path('movementlist/<int:year>/<int:month>', MovementMontArchiveView.as_view(), name='finances_month'),
    path('movementresume', UserAccountingTemplateView.as_view(), name='finances_resume'),
    path('edit/<int:pk>', MovementUpdateView.as_view(), name = 'finances_edit'),
    path('delete/<int:pk>', MovementDeleteView.as_view(), name='finances_delete'),
    path('workerstime', WorkersTimeListView.as_view(), name='workerstime_list'),
    #path('attendance', AttendanceReport.as_view(), name='attendance_report'),
    path(
        'attendance/<str:inicio>/<str:final>/<str:extra>/<str:adicional>',
        AttendaceFullReportClass.as_view(),
        name='attendance_report'),
    path('attendanceform', AttendanceReportFormClass.as_view(), name='attendance_form'),
    path(
        'projectattendance/<int:pk>',
        ProjectAttendanceReport.as_view(),
        name='projectattendance_report'
    ),
]