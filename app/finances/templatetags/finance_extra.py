import os
from django import template
from django.contrib.auth.models import Group

register = template.Library()

@register.filter
def filename(value):
    try:
        fullname = os.path.basename(value.file.name)
    except:
        fullname = 'No encontrado'
    short_name = fullname[:5] + '...' + fullname[-10:]
    return short_name

@register.filter(name='convert_data_frame_to_html_table_rows')
def convert_data_frame_to_html_table_headers(df):
    html = "<tr>"
    for col in df.columns:
        html += f"<th>{col}</th>"
    html += "</tr>"
    return html

@register.filter(name='convert_data_frame_to_html_table_headers')
def convert_data_frame_to_html_table_rows(df):
    html = ""
    for row in df.values:
        row_html = "<tr>"
        for value in row:
            row_html += f"<td>{value}</td>"
        row_html += "</tr>"
    html += row_html
    return html