from typing import Any
from django.db.models import Sum, Value, Avg
from django.http import HttpResponse

from django.views.generic import (
    CreateView, UpdateView, DeleteView, ListView, TemplateView,
    MonthArchiveView, FormView, RedirectView
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from finances.models import Movement
from projects.models import Project, Report
from finances.forms import MovementCreateForm, DateSelectForm, AttendanceForm
from django.contrib.auth.models import User

import datetime

import pandas as pd

class MovementCreateView(LoginRequiredMixin,CreateView):
    model = Movement
    #fields = '__all__'
    form_class = MovementCreateForm
    success_url = reverse_lazy("finances:finances_home")

    def get_form_kwargs(self):
        kwargs = super(MovementCreateView, self).get_form_kwargs()
        u = self.request.user
        kwargs['initial']= {
            'responsable':u.pk,
        }
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context =  super(MovementCreateView, self).get_context_data(*args, **kwargs)
        context['no_invoice'] = Movement.objects.filter(
            no_invoice = True,
            responsable = self.request.user.pk,
            creation_date__month = datetime.datetime.now().month,
        ).order_by('creation_date')
        context['invoice'] = Movement.objects.filter(
            no_invoice = False,
            responsable = self.request.user.pk,
            creation_date__month = datetime.datetime.now().month,
        ).order_by('creation_date')
        context['total_fact'] = context['invoice'].filter(
            no_invoice=False
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        context['total_no_fact'] = context['no_invoice'].filter(
            no_invoice = True
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        return context

class MovementUpdateView(LoginRequiredMixin, UpdateView):
    model = Movement
    #fields = '__all__'
    form_class = MovementCreateForm
    success_url = reverse_lazy("finances:finances_home")

    def get_context_data(self, *args, **kwargs):
        context =  super(MovementUpdateView, self).get_context_data(*args, **kwargs)
        context['no_invoice'] = Movement.objects.filter(
            no_invoice = True,
            responsable = self.request.user.pk,
            creation_date__month = datetime.datetime.now().month,
        ).order_by('creation_date')
        context['invoice'] = Movement.objects.filter(
            no_invoice = False,
            responsable = self.request.user.pk,
            creation_date__month = datetime.datetime.now().month,
        ).order_by('creation_date')
        context['total_fact'] = context['invoice'].filter(
            no_invoice=False
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        context['total_no_fact'] = context['no_invoice'].filter(
            no_invoice = True
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        return context

class MovementDeleteView(LoginRequiredMixin, DeleteView):
    model = Movement
    success_url = reverse_lazy("finances:finances_home")

class MovementAdminListView(LoginRequiredMixin, ListView):
    model = Movement
    context_object_name = 'movement_list'

class MovementMontArchiveView(LoginRequiredMixin, MonthArchiveView):
    model = Movement
    date_field = 'creation_date'
    year_format = '%Y'
    month_format = '%m'
    context_object_name ="movement_list"
    allow_empty = True
    allow_future = True

    def get_context_data(self, *args, **kwargs):
        context =  super(MovementMontArchiveView, self).get_context_data(*args, **kwargs)
        #print(self.request.GET.get('order_by'))
        if self.request.GET.get('order_by'):
            order_by = self.request.GET.get('order_by')
        else:
            order_by = 'creation_date'
        if self.request.GET.get('responsable'):
            responsable = self.request.GET.get('responsable')
        else:
            responsable = self.request.user.pk
        context['no_invoice'] = context['object_list'].filter(
            no_invoice = True,
            responsable = responsable
        ).order_by(order_by)
        context['invoice'] = context['object_list'].filter(
            no_invoice = False,
            responsable = responsable
        ).order_by(order_by)
        context['total_fact'] = context['invoice'].filter(
            no_invoice=False
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        context['total_no_fact'] = context['no_invoice'].filter(
            no_invoice = True
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        return context

class UserAccountingTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'finances/useraccounting.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['resumen'] = User.objects.annotate(total = Sum('movement__amount')).filter(total__gt = 0)
        return context

class MovementLastMonth(LoginRequiredMixin, RedirectView):
    

    def get_redirect_url(self, *args: Any, **kwargs: Any) -> str | None:
        actual_date = datetime.date.today()
        url = 'movementlist/{}/{}?responsable={}'.format(
            actual_date.year,
            actual_date.month,
            self.request.GET.get('responsable')
        )
        return url
    # url=reverse_lazy("finances:finances_month", kwargs={
    #     'year':actual_date.year,
    #     'month':actual_date.month,
    # })
'''
Lista de tiempo dedicado por proyecto
'''
class WorkersTimeListView(LoginRequiredMixin, FormView):
    form_class = DateSelectForm
    template_name = 'finances/workers_time.html'
    success_url = reverse_lazy("finances:workerstime_list")
    extra_context = dict()

    def form_valid(self, form):
        projects = Project.objects.all()
        time_proyect_report = []
        inicio = form.cleaned_data["init_date"]
        final = form.cleaned_data["end_date"]
        #print('de {} hasta {}'.format(inicio, final))
        for project in projects:
            if project.activity_set.all():
                for act in project.activity_set.all():
                    for report in act.report_set.filter(date__range=[inicio, final]):
                        for u in report.workers.all():
                            time_proyect_report.append({
                                'project':'CC-INX-{}: {}'.format(project.cc_number, project.name),
                                'user':'{} {}'.format(u.first_name, u.last_name),
                                'time':report.duration
                            })
        df = pd.DataFrame(time_proyect_report)
        #print(df)
        df2 = df.groupby(['project','user'])['time'].sum()
        df3 = df.groupby(['project'])['time'].sum()
        self.extra_context['project_user'] = df2
        self.extra_context['project_time'] = df3
        self.extra_context['inicio'] = inicio
        self.extra_context['final'] = final
        return super().form_valid(form)
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        try:
            context['project_user'] = self.extra_context['project_user']
            context['project_time'] = self.extra_context['project_time']
            context['inicio'] = self.extra_context['inicio']
            context['final'] = self.extra_context['final']
            #print(context)
        except:
            projects = Project.objects.all()
            time_proyect_report = []
            for project in projects:
                if project.activity_set.all():
                    for act in project.activity_set.all():
                        for report in act.report_set.all():
                            for u in report.workers.all():
                                time_proyect_report.append({
                                    'project':'CC-INX-{}: {}'.format(project.cc_number, project.name),
                                    'user':'{} {}'.format(u.first_name, u.last_name),
                                    'time':report.duration
                                })
            df = pd.DataFrame(time_proyect_report)
            #print(df)
            df2 = df.groupby(['project','user'])['time'].sum()
            df3 = df.groupby(['project'])['time'].sum()
            context['project_user'] = df2
            context['project_time'] = df3
        result = []
        for key in context['project_time'].keys():
            df4 = context['project_user'].filter(like=key)
            trabajadores = []
            for index in df4[key].index:
                trabajadores.append({
                    'name':index,
                    'time':df4[key][index],
                })
            result.append({
                'nombre_de_proyecto':key,
                'horas_de_proyecto':context['project_time'][key],
                'horas_por_trabajador':trabajadores,
            })
        context['result'] = result
        return context
    
class AttendanceReport(LoginRequiredMixin, FormView):
    form_class = DateSelectForm
    template_name = "finances/attendance.html"
    success_url = reverse_lazy("finances:attendance_report")
    extra_context = dict()

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        delta = datetime.timedelta(days=1)
        users = User.objects.filter(is_active=True).order_by('last_name')
        calendar = []
        report_dates = []
        try:
            start_date = datetime.datetime.strptime(
                self.extra_context['inicio'],
                '%Y-%m-%d'
            )
            end_date = datetime.datetime.strptime(
                self.extra_context['final'],
                '%Y-%m-%d'
            ) 
        except:
            start_date = datetime.date(2023, 1, 1)
            end_date = datetime.date(2023, 1, 1)
        start_dt = start_date
        #print(start_dt)
        while start_dt <= end_date:
            report_dates.append(start_dt)
            start_dt += delta
        context['days'] = report_dates
        for user in users:
            result = []
            start_dt = start_date
            #print(user)
            while start_dt <= end_date:
                reports = user.report_workers.filter(date=start_dt)
                result.append(sum([r.duration for r in reports if r.duration is not None]))     
                start_dt += delta
            if sum(result) > 0:           
                calendar.append({
                    'user':user,
                    'hours':result
                })
        self.extra_context['inicio'] = None
        context['calendar'] = calendar
        return context

    def form_valid(self, form):
        self.extra_context['inicio'] = form.cleaned_data["init_date"]
        self.extra_context['final'] = form.cleaned_data["end_date"]
        return super().form_valid(form)

class AttendanceReportFormClass(LoginRequiredMixin, FormView):
    form_class = AttendanceForm
    template_name = 'finances/attendance_form.html'

    def form_valid(self, form):
        url_inicio = form.cleaned_data["init_date"]
        url_final = form.cleaned_data["end_date"]
        url_extra = form.cleaned_data["extra_time"]
        url_adicional = form.cleaned_data["adicionales"]
        #print("{}, {}, {}, {}".format(url_inicio, url_final, url_extra, url_adicional))
        self.success_url = reverse_lazy(
            "finances:attendance_report",
            kwargs = {
                'inicio':url_inicio,
                'final':url_final,
                'extra':url_extra,
                'adicional':url_adicional,
            }
        )
        return super().form_valid(form)

class AttendaceFullReportClass(LoginRequiredMixin, TemplateView):
    template_name = 'finances/attendance_report.html'
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        delta = datetime.timedelta(days=1)
        users = User.objects.all().order_by('last_name')
        calendar = []
        report_dates = []
        try:
            start_date = datetime.datetime.strptime(
                self.kwargs['inicio'],
                '%Y-%m-%d'
            )
            end_date = datetime.datetime.strptime(
                self.kwargs['final'],
                '%Y-%m-%d'
            ) 
        except:
            start_date = datetime.date(2023, 1, 1)
            end_date = datetime.date(2023, 1, 1)
        start_dt = start_date
        #print(start_dt)
        while start_dt <= end_date:
            report_dates.append(start_dt)
            start_dt += delta
        context['days'] = report_dates
        for user in users:
            result = []
            start_dt = start_date
            #print(user)
            while start_dt <= end_date:
                if self.kwargs['extra'] == 'True':
                    reports = user.report_workers.filter(date=start_dt, extratime=True)
                elif self.kwargs['adicional'] == 'True':
                    reports = user.report_workers.filter(date=start_dt, activity__type=0)
                else:
                    reports = user.report_workers.filter(date=start_dt)
                result.append(sum([r.duration for r in reports if r.duration is not None]))     
                start_dt += delta
            if sum(result) > 0:           
                calendar.append({
                    'user':user,
                    'hours':result
                })
        context['calendar'] = calendar
        return context
    
class ProjectAttendanceReport(LoginRequiredMixin, TemplateView):
    template_name = 'finances/project_attendance.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        calendar_normal = []
        calendar_extra = []
        report_dates = []
        delta = datetime.timedelta(days=1)
        project_pk = self.kwargs['pk']
        project_reports = Report.objects.filter(activity__project = project_pk).order_by('date')
        start_date = project_reports[0].date
        end_date = project_reports[len(project_reports)-1].date
        start_dt = start_date
        total_horas_normal = 0.0
        total_horas_extra = 0.0
        total_costo_normal = 0.0
        total_costo_extra = 0.0
        u_list = []
        [[u_list.append(w) for w in r.workers.all()] for r in project_reports]
        #print(set(u_list))
        while start_dt <= end_date:
            report_dates.append(start_dt)
            start_dt += delta
        context['days'] = report_dates
        for user in set(u_list):
            result_normal = []
            result_extra = []
            start_dt = start_date
            while start_dt <= end_date:
                reports_normal = user.report_workers.filter(date=start_dt,  extratime=False, activity__project=project_pk)
                reports_extra = user.report_workers.filter(date=start_dt, extratime=True, activity__project=project_pk)
                result_normal.append(sum([r.duration for r in reports_normal if r.duration is not None]))     
                result_extra.append(sum([r.duration for r in reports_extra if r.duration is not None])) 
                start_dt += delta
            sum_normal = sum(result_normal)
            if sum_normal > 0:  
                result_normal.append(sum_normal)      
                result_normal.append(sum_normal * user.workercost.normal_cost)  
                total_horas_normal += float(sum_normal)
                total_costo_normal += float(sum_normal * user.workercost.normal_cost)
                calendar_normal.append({
                    'user':user,
                    'hours':result_normal
                })
            sum_extra = sum(result_extra)
            if sum_extra > 0:
                result_extra.append(sum_extra)
                result_extra.append(sum_extra*user.workercost.extra_cost)
                total_horas_extra += float(sum_extra)      
                total_costo_extra += float(sum_extra*user.workercost.extra_cost) 
                calendar_extra.append({
                    'user':user,
                    'hours':result_extra
                })
        
        context['proyecto'] = Project.objects.get(pk=project_pk)
        context['total_horas_normal'] = total_horas_normal
        context['total_horas_extra'] = total_horas_extra
        context['total_costo_normal'] = total_costo_normal
        context['total_costo_extra'] = total_costo_extra
        context['total_horas'] = total_horas_normal + total_horas_extra
        context['total_costo'] = total_costo_normal + total_costo_extra
        context['table_col'] = len(result_normal)-1
        context['calendar_normal'] = calendar_normal
        context['calendar_extra'] = calendar_extra
        return context