from django import forms
from django.forms import ModelChoiceField
from django.contrib.auth.models import User
from profiles.models import Supplier
from datetime import datetime
from finances.models import Movement

class UserModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.get_full_name()

class MovementCreateForm(forms.ModelForm):
    responsable = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
        widget = forms.Select(),
        label = 'Responsable',
    )
    class Meta:
        model = Movement
        fields = '__all__'
        widgets = {
            'payee':forms.Select(attrs={'class':'form-control'}),
            'creation_date':forms.DateInput(attrs={'class':'form-control datepicker','type':'date'}),
            'description':forms.Textarea(),
            'amount':forms.NumberInput(attrs={'class':'form-control'}),
            'no_invoice':forms.CheckboxInput(attrs={'class':'form-control'}),
            'payment':forms.CheckboxInput(attrs={'class':'form-control'}),
            'responsable':forms.Select(),
            'files':forms.FileInput(attrs={}),
        }
        labels = {
            'payee':'Proveedor',
            'creation_date':'Fecha de compra',
            'description':'Descripción',
            'amount':'Monto',
            'no_invoice':'Sin factura',
            'payment':'Pagado',
            'files':'Factura',
        }

class DateSelectForm(forms.Form):
    init_date = forms.CharField(
        label = 'Fecha de inicio: ',
        widget = forms.DateInput(attrs={'class':'form-control datepicker','type':'date'})
    )
    end_date = forms.CharField(
        label = 'Fecha final: ',
        widget = forms.DateInput(attrs={'class':'form-control datepicker','type':'date'})
    )
    
class AttendanceForm(forms.Form):
    init_date = forms.CharField(
        label = 'Fecha de inicio: ',
        widget = forms.DateInput(attrs={'class':'form-control datepicker','type':'date'})
    )
    end_date = forms.CharField(
        label = 'Fecha final: ',
        widget = forms.DateInput(attrs={'class':'form-control datepicker','type':'date'})
    )
    extra_time = forms.BooleanField(
        label = 'Solo tiempo extra',
        widget = forms.NullBooleanSelect(),
        initial = False,
        required = False,
    )
    adicionales = forms.BooleanField(
        label = 'Solo actividades adicionales',
        widget = forms.NullBooleanSelect(),
        initial = False,
        required=False,
    )