from django.db import models
from django.contrib.auth.models import User
from profiles.models import Supplier
from datetime import datetime

import os

class Movement(models.Model):
    payee = models.ForeignKey(
        Supplier,
        models.SET_NULL,
        blank = True,
        null = True
    )
    creation_date = models.DateField(
        default = datetime.now,
    )
    description = models.CharField(
        max_length=254
    )
    amount = models.DecimalField(
        decimal_places=3,
        max_digits=7
    )
    no_invoice = models.BooleanField(
        blank = True,
        default = False
    )
    payment = models.BooleanField(
        blank = True,
        default = False
    )
    responsable = models.ForeignKey(
        User,
        models.CASCADE
    )
    files = models.FileField(
        upload_to="finances/%Y/%m/%d/",
        blank = True,
        null = True
    )