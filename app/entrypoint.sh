#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Esperando por postgres"

    while ! nc -z $SQL_HOST $SQL_PORT; do
        sleep 0.1
    done

    echo "Postgres iniciado"
fi

#python manage.py flush --no-input
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --noinput --clear

exec "$@"