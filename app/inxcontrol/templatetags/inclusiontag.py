from django import template

register = template.Library()


@register.inclusion_tag('common_menu.html')
def show_common_menu(button_select=None):
    if button_select == 1:
        button = {
            'svg_icon':'<svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 50 50" width="15px" height="15px" baseProfile="basic"><path fill="#ffd217" d="M2.652,47.712l3.636-16.365l30.11-30.11l12.729,12.728l-30.111,30.11L2.652,47.712z M9.94,33.351	l-2.02,9.092l9.091-2.021L43.47,13.964l-7.071-7.071L9.94,33.351z"/><polygon fill="#af87ce" points="8.238,42.372 7.92,42.442 7.991,42.125 4.638,38.772 2.652,47.711 11.591,45.725"/><polygon fill="#2dea55" points="40.641,22.449 32.863,14.671 35.691,11.843 40.641,16.793 43.47,13.964 36.398,6.893 30.742,12.55 27.914,9.722 36.398,1.236 49.127,13.964"/><rect width="4" height="4" x="38.641" y="17.621" fill="#0cbc35" transform="rotate(-45.001 40.641 19.622)"/><rect width="4" height="14" x="12.479" y="28.884" fill="#ffd217" transform="rotate(-45.001 14.478 35.885)"/></svg>'
        }
    elif button_select == 2:
        button = {
            'svg_icon':'<svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" enable-background="new 0 0 48 48"><rect x="7" y="4" fill="#BBDEFB" width="34" height="40"/><g fill="#2196F3"><rect x="13" y="26" width="4" height="4"/><rect x="13" y="18" width="4" height="4"/><rect x="13" y="34" width="4" height="4"/><rect x="13" y="10" width="4" height="4"/><rect x="21" y="26" width="14" height="4"/><rect x="21" y="18" width="14" height="4"/><rect x="21" y="34" width="14" height="4"/><rect x="21" y="10" width="14" height="4"/></g></svg>'
        }
    else:
        button = {
            'svg_icon':'<svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 50 50" width="15px" height="15px" baseProfile="basic"><rect width="42" height="4" x="4" y="5" fill="#16d6fa"/><rect width="4" height="7" x="23" y="1" fill="#16d6fa"/><rect width="4" height="27" x="15" y="13" fill="#16d6fa"/><rect width="4" height="27" x="23" y="13" fill="#16d6fa"/><rect width="4" height="27" x="31" y="13" fill="#16d6fa"/><path fill="#16d6fa" d="M41.949,48H8.051L6.948,5h36.104L41.949,48z M11.949,44h26.102l0.897-35H11.052L11.949,44z"/><rect width="4" height="4" x="23" y="5" fill="#00a4ff"/></svg>'
        }