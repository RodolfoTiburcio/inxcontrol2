import pandas as pd
from django.contrib.auth.models import User
from profiles.models import Client
from projects.models import Project
from datetime import datetime
df = pd.read_csv('./inxcontrol/timelog_2.csv',sep=';')

# Run this program with: exec(open("./inxcontrol/issues_analisys.py").read())
# in a container django shell
'''
Funcion para agregar a la aplicación una lista de clientes desde
la lista de proyectos generada por airtable
'''
'''
Index(['Proyecto', 'Fecha', 'Creado', 'Semana', 'Autor', 'Usuario',
       'Actividad', 'Petición', 'Tipo', 'Estado', 'Categoría',
       'Versión prevista', 'Comentario', 'Horas', 'Personal', 'Cliente',
       'Tiempo muerto (min)', 'Cliente.1'],
      dtype='object')

'REPORTE DIARIO SCC-INX-SEPTIEMBRE-2023'
 'REPORTE DIARIO SCC-INX AGOSTO-2023'
 'CC-INX-303 SUMINISTRO E INSTALACION DE CAMARAS IP'
 'REPORTE DIARIO SCC-INX-JULIO-2023' 'CC-INX-313 MOVIMIENTO DE MEZCLADORA'
 'CC-INX-1000 AUTOMATIZACION DE REPORTES'
 'REPORTE DIARIO SCC-INX- JUNIO-2023'
 'CC-INX-321 DREAMREPORT BASCULA DE DESCARGA Y PANTALLA INFORMACION'
 'CC-INX-290_FLOCULADOR' 'CC-INX-246 TRANSFORMADOR TIPO SUBESTACION'
 'CC-INX-251 ACTUALIZACION TANQUES DE LOTEO'
 'REPORTE DIARIO SCC-INX-MAYO-2023' 'CC-INX-273 NUEVA NAVE DE PROCESO'
 'CC-INX-288 RETIRO DE CABLEADO DE FIBRA PUENTE BC-3'
 'CC-INX-148 REMOTO AD, VISUALIZACION BASC, INT DELTA P'
 'CC-INX-279 INSTALACION DE BASCULA SIGMA '
 'CC-INX-286 MIGRACION DE LINEAS PAQUETINES PETERS 2'
 'CC-INX-189 RETROFIT DE FRESADORA' 'CC-INX-185 ACTUALIZACION LICENCIA'
 'CC-INX-266 DEMAR' 'REPORTE DIARIO SCC-INX ABRIL-2023 '
 'SCC-INX-100 REHABILITACION E INTERCAMBIO DE FUJOMETRO'
 'CC-INX-226 PLC EMERSON RX3i' 'CC-INX-281 REPARACION BOMBA GOULDS 50 HP']

['Raymundo O.' 'Rafael A.' 'Joel R.' 'Rodolfo B.' 'Rodolfo T.' 'Sergio p.'
 'Adrian I.' 'Isai H.' 'Wilber E.']
'''


if __name__=='builtins':
    #print(df.iloc[0]['Petición'])
    print(df['Petición'].unique())