import csv
from reports.models import Report
from django.contrib.auth.models import User

clientes_id = {
    'CARGILL':1,
    'SSA':2,
    'MASECA':3,
    'CUETARA':5,
    'SAN ANTONIO':4,
    'OFICINA':6,
    'CHEDRAUI':7,
    'PREACERO':8,
    'ICAVE':9,
    '':10
}
responsables_id = {
    'JRM':5,
    'RTC':1,
    'RBB':4,
    'EEE':7,
    'LFRD':6
}

responsables = {}
ayudantes = {}
clientes = {}
proyectos = {}
responsable_count = 1
ayudante_count = 1
cliente_count = 1
proyecto_count = 1
with open('./reportapp/reports.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter = ',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(row)
            line_count += 1
        else:
            """
            # Esto funciona aqui, para identificar los elementos de la
            # seccion a analizar:
            """
            if not row[3] in responsables:
                responsables[row[3]] = responsable_id
                responsable_id +=  1
            if not row[2] in clientes:
                clientes[row[2]] = cliente_id
                cliente_id += 1
            multiproyecto = row[8].split(',')
            for single_project in multiproyecto:
                if single_project:
                    if not single_project in proyectos:
                        proyectos[single_project] = proyecto_id 
                        proyecto_id += 1
            multiayudantes = row[4].split(',')
            for single_ayudante in multiayudantes:
                if not single_ayudante in ayudantes:
                    ayudantes[single_ayudante] = ayudante_id
                    ayudante_id += 1
    print(ayudantes)
    print(clientes)
    print(proyectos)
    print(responsables)
    for cliente in clientes:
        print(f'nombre:{cliente}, id:{clientes_id[cliente]}')
    for proyecto in proyectos:
        caracter_no = 0
        for caracter in proyecto:
            if caracter == ' ':
                primer_espacio = caracter_no
                break
            else:
                caracter_no += 1
        print(proyecto.replace(proyecto[0:caracter_no+1], ''))
from reports.models import Report
report = Report.objects.get(pk=1)
print(report.description)
print(csv.list_dialects())
