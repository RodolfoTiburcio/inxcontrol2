from projects.models import Photo
from PIL import Image
from io import StringIO
import os

from django.core.files.uploadedfile import UploadedFile
# Run this program with: exec(open("./inxcontrol/extras.py").read())
# in a container django shell

def create_thumbnails():
    THUMBNAIL_SIZE = (99, 66)
    PIL_TYPE = ''
    DJANGO_TYPE = ''
    photos = Photo.objects.all()

    for photo in photos:
        a, b = os.path.splitext(photo.pictures.path)
        file_extension = b[1:]
        if file_extension == 'jpg':
            PIL_TYPE = 'jpeg'
            DJANGO_TYPE = 'image/jpeg'
        elif file_extension == 'png':
            PIL_TYPE = 'png'
            DJANGO_TYPE = 'image/png'
        if file_extension in ['jpg', 'png']:
            with photo.pictures.open() as original:
                #print(original)
                image = Image.open(original)
                image.thumbnail(THUMBNAIL_SIZE)
                temp_handle = StringIO()
                image.save('temp', format = PIL_TYPE)
                with open('temp', 'rb') as thum:
                    photo.thumbnail.save(
                        original.name,
                        UploadedFile(
                            file = thum,
                            content_type=DJANGO_TYPE
                        )
                    )

def bu_database():
    pass

if __name__ == 'builtins':
    create_thumbnails()