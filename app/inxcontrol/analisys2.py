import csv
from reports.models import Report
from clients.models import Client
from projects.models import Project
from django.contrib.auth.models import User

Report.objects.all().delete()
Project.objects.all().delete()
Client.objects.all().delete()
User.objects.all().delete()

User.objects.create_user(username = 'tiburcio', password = '120d0lf0')

usuarios = []
clientes = []
proyectos = {}
nombres_proy = []
reports = []

"""
Replazar algúnos textos para simplificar depues la identificación de
nombres de proyectos:
"""
#print('open file')
with open('./reportapp/reports.csv', 'r') as file:
    file_data = file.read()

#print('edit file')
file_data = file_data.replace('_Interno', 'CC-INX-FF Interno')
file_data = file_data.replace('CC-INX-00 OFICINA', 'CC-INX-FF OFICINA')
file_data = file_data.replace('mvs', 'MVS')
file_data = file_data.replace(
    'CC-INX-158 MANTTO A MACROPOSTES,CC-INX-138 ACTUALIZACION ARCO DE LAVADO 2',
    'CC-INX-158 MANTTO A MACROPOSTES'
)
file_data = file_data.replace(
    'CC-INX-158 MANTTO A MACROPOSTES,CC-INX-156 ARGOLLAS TAPAS BASCULAS,CC-INX-146 SERV',
    'CC-INX-158 MANTTO A MACROPOSTES')
file_data = file_data.replace(
    'CC-INX-42- MANTT. SISTEMA DE TRATAMIENTO DE AGUA SSA ,CC-INX-121 ARRANCADORES SUAVES 300HP Y 250HP',
    'CC-INX-42- MANTT. SISTEMA DE TRATAMIENTO DE AGUA SSA'
)
file_data = file_data.replace(
    'CC-INX-158 MANTTO A MACROPOSTES,CC-INX-131 DRIVES TORRES DE MUELLE,CC-INX-121 ARRANCADORES SUAVES 300HP Y 250HP,CC-INX-42- MANTT. SISTEMA DE TRATAMIENTO DE AGUA SSA ',
    'CC-INX-158 MANTTO A MACROPOSTES'
)
file_data = file_data.replace(
    'CC-INX-158 MANTTO A MACROPOSTES,CC-INX-42- MANTT. SISTEMA DE TRATAMIENTO DE AGUA SSA ',
    'CC-INX-158 MANTTO A MACROPOSTES'
)
file_data = file_data.replace(
    'CC-INX-158 MANTTO A MACROPOSTES,CC-INX-138 ACTUALIZACION ARCO DE LAVADO 2',
    'CC-INX-158 MANTTO A MACROPOSTES'
)
file_data = file_data.replace(
    'CC-INX-158 MANTTO A MACROPOSTES,CC-INX-156 ARGOLLAS TAPAS BASCULAS,CC-INX-146 SERV',
    'CC-INX-158 MANTTO A MACROPOSTES'
)

#rint('save file')  
with open('./reportapp/reports.csv', 'w') as file:
    file.write(file_data)

#print('analize file')
with open('./reportapp/reports.csv') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    #print('open file as csv')
    for row in csv_reader:
        if not row['Responsable'] in usuarios:
            usuarios.append(row['Responsable'])
        if not row['Cliente'] in clientes:
            clientes.append(row['Cliente'])
        if not row['Proyecto'] in proyectos:
            proyectos[row['Proyecto']] = {
                'client':row['Cliente'],
                'respon':row['Responsable'],
            }
        multiayudantes = row["Ayudante(s)"].split(',')
        for single_ayudante in multiayudantes:
            if not single_ayudante in usuarios:
                usuarios.append(single_ayudante)
    for proyecto in proyectos:
        caracter_no = 0
        for caracter in proyecto:
            if caracter == ' ':
                primer_espacio = caracter_no
                break
            else:
                caracter_no += 1
        nombres_proy.append({
            'proy':proyecto.replace(proyecto[0:caracter_no+1],''),
            'client':proyectos[proyecto]['client'],
            'respon':proyectos[proyecto]['respon'],
        }
        )
    """
    Simple User creation:
    """
    #print('create users')
    for usuario in usuarios:
        if usuario:
            try:
                #print(f'create {usuario} user')
                u = User.objects.create_user(username = usuario, password='d3f4ult321')
                u.save()
            except:
                print(f'Usuario {usuario} no creado')
    """for usuario in usuarios:
        username = input(f'Nombre de usuario para {usuario}: ')
        first_name = input(f'Nombde para {usuario}: ')
        last_name = input(f'Apellido para {usuario}: ')
        u = User.objects.create_user(username)
        u.first_name = first_name
        u.last_name = last_name
        u.save()"""
    for cliente in clientes:
        if cliente:
            try:
                #print(f'creando {cliente} client')
                c = Client(name = cliente)
                c.save()
                #print(f'cliente {c.name} creado')
            except:
                print(f'cliente {cliente} no creado')
    for nombre_proy in nombres_proy:
        if nombre_proy['proy']:
            #print(f'proy: {nombre_proy["proy"]}, {nombre_proy["client"]}, {nombre_proy["respon"]}')
            try:
                p = Project()
                p.name = nombre_proy['proy']
                try:
                    p.client = Client.objects.get(name = nombre_proy['client'])
                except:
                    print(f'No se encuentra cliente {nombre_proy["client"]} para {nombre_proy["proy"]}')
                try:
                    p.manager = User.objects.get(username = nombre_proy['respon'])
                except:
                    print(f'No se encuentra admin {nombre_proy["respon"]} para {nombre_proy["proy"]}')
                p.save()
                #print(f'Proyecto {p.name} creado')
            except:
                print(f'No se pudo crear el proyecto {p.name}')
with open('./reportapp/reports.csv') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        r = Report()
        try:
            r.responsable = User.objects.get(username = row['Responsable'])
        except:
            print(f'no se encontro el responsable {row["Responsable"]}')
        r.description = row['Detalle de la actividad']
        r.date = row['Date']
        r.name = row['Nombre de la actividad']
        r.notes = row['Notas']
        try:
            r.client = Client.objects.get(name = row['Cliente'])
        except:
            print(f'no se creo el cliente {row["Cliente"]}')
        if row['Tiempo dedicado']:
            hours = float(row['Tiempo dedicado'])
        if hours > 99.99:
            hours = hours/60
        r.duration = hours
        for nombre_proy in nombres_proy:
            if nombre_proy['proy']:
                if nombre_proy['proy'] in str(row['Proyecto']):
                    #print(nombre_proy['proy'])
                    try:
                        r.project = Project.objects.get(name = nombre_proy['proy'])
                    except:
                        print(f'No se encontro el proyecto {nombre_proy["proy"]}')
        multiayudantes = row["Ayudante(s)"].split(',')
        r.save()
        for single_ayudante in multiayudantes:
            try:
                r.workers.add(User.objects.get(username = single_ayudante))
                #print(f'add worker {single_ayudante}')
            except:
                print(f'No se encontro el ayudante {single_ayudante} para reporte {r.id}')
        #print(f'report: {r.project} {r.responsable}, {r.description}, {r.duration}, {r.date}')
        #print('reporte creado')

"""
Para ejecutar el programa desde django shell:
exec(open("./reportapp/analisys2.py").read())

Columnas de la tabla de reportes:
ID,
-Date,
-Cliente,
-Responsable,
-Ayudante(s),
-Nombre de la actividad,
-Detalle de la actividad,
-Notas,
-Proyecto,
-Tiempo dedicado

"""