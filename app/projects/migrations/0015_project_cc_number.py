# Generated by Django 4.2.4 on 2023-09-16 02:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0014_rename_type_activity_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='cc_number',
            field=models.IntegerField(blank=True, help_text='Numero de centro de costo', null=True, unique=True),
        ),
    ]
