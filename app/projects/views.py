from typing import Any
from django.db.models.query import QuerySet
from django.shortcuts import render
from django.views.generic import ( CreateView, ListView, DetailView, UpdateView, DeleteView,
                                   FormView, TemplateView, DayArchiveView, RedirectView )
from django.db.models import Max, Sum, Case, Value, When, F
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Project, Activity, Requirements, Report, Photo, ActivityProgress, ReportProgress
from django.contrib.auth.models import User
from .forms import (ProjectCreateForm, ActivityCreateForm, RequirementCreateForm,
                    ReportCreateForm, PhotoForm, ActivityprogressForm, ReporprogressForm)

import os, datetime
from PIL import Image

from django.core.files.uploadedfile import UploadedFile

class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    form_class = ProjectCreateForm
    
    def get_success_url(self) -> str:
        return reverse_lazy('projects:project_detail', kwargs={'pk':self.object.pk})

    def get_form_kwargs(self):
        kwargs = super(ProjectCreateView, self).get_form_kwargs()
        u = self.request.user
        try:
            new_cc_number = Project.objects.all().aggregate(Max('cc_number'))['cc_number__max']+1
        except:
            new_cc_number = 1
        kwargs['initial'] = {
            'manager':u.pk,
            'cc_number':new_cc_number,
        }
        return kwargs
    
class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    context_object_name = 'project_list'
    paginate_by = 20
    
    def get_queryset(self):
        queryset = super().get_queryset().order_by('-cc_number')
        if self.request.GET.get('status'):
            queryset = queryset.filter(
                status = self.request.GET.get('status')
            )
        #print(queryset)
        return queryset
    
class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    context_object_name = 'project'

class ProjectUpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    form_class = ProjectCreateForm
    success_url = reverse_lazy('projects:project_list')

    def get_success_url(self):
        return reverse_lazy('projects:project_detail', kwargs = {'pk':self.kwargs['pk']})

class ActivityListView(LoginRequiredMixin, ListView):
    model = Activity
    context_object_name = 'activity_list'
    paginate_by=10

    def get_queryset(self) -> QuerySet[Any]:
        if self.request.GET.get('type'):
            type_id = int(self.request.GET.get('type').strip('"'))
            queryset = Activity.objects.filter(
                type = type_id,
            ).order_by('-pk')
        elif self.request.GET.get('status') == '1':
            queryset = Activity.objects.filter(
                status__lte = 1
            ).order_by('-pk')
        elif self.request.GET.get('status') == '0':
            queryset = Activity.objects.all().order_by('-pk')
        else:
            queryset = Activity.objects.filter(
                status__lte = 1,
                incharge = self.request.user.pk
            ).order_by('-pk')
        return queryset

    # def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
    #     context = super().get_context_data(**kwargs)
    #     if self.request.GET.get('type'):
    #         type_id = int(self.request.GET.get('type').strip('"'))
    #         context['activity_list'] = Activity.objects.filter(
    #             type = type_id,
    #         ).order_by('-pk')
    #     elif self.request.GET.get('status') == '1':
    #         context['activity_list'] = Activity.objects.filter(
    #             status__lte = 1
    #         ).order_by('-pk')
    #         #print('Entrada')
    #     elif self.request.GET.get('status') == 0:
    #         context['activity_list'] = Activity.objects.all().order_by('-pk')
    #     else:
    #         context['activity_list'] = Activity.objects.filter(
    #             status__lte = 1,
    #             incharge = self.request.user.pk
    #         ).order_by('-pk')
    #     return context
    
class ActivityPrintView(LoginRequiredMixin, TemplateView):
    template_name = 'projects/activity_print.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['activity_list'] = [a for a in Activity.objects.all().order_by('project') if a.report_set.all()]
        return context

class ActivityCompactListView(LoginRequiredMixin, ListView):
    model = Activity
    paginate_by=20
    template_name = 'projects/activity_compact_list.html'

    def get_queryset(self) -> QuerySet[Any]:
        if self.request.GET.get('type'):
            type_id = int(self.request.GET.get('type').strip('"'))
            queryset = Activity.objects.filter(
                type = type_id,
            ).order_by('-pk')
        elif self.request.GET.get('status') == '1':
            queryset = Activity.objects.filter(
                status__lte = 1
            ).order_by('-pk')
        elif self.request.GET.get('status') == '0':
            queryset = Activity.objects.all().order_by('-pk')
        else:
            queryset = Activity.objects.filter(
                status__lte = 1,
                incharge = self.request.user.pk
            ).order_by('-pk')
        return queryset

class ActivityCreateView(LoginRequiredMixin, CreateView):
    model = Activity
    form_class = ActivityCreateForm

    def get_success_url(self):
        return reverse_lazy('projects:activity_detail', kwargs={'pk':self.object.pk})

    def get_form_kwargs(self):
        kwargs = super(ActivityCreateView, self).get_form_kwargs()
        u = self.request.user
        kwargs['initial'] = {
            'incharge':u.pk,
        }
        project_pk = self.request.GET.get('project')
        if project_pk:
            kwargs['initial']['project'] = project_pk
        return kwargs

class ActivityDetailView(LoginRequiredMixin, DetailView):
    model = Activity
    context_object_name = 'activity'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        act = context['object']
        progress = []
        for p in act.activityprogress_set.all():
            suma = ReportProgress.objects.filter(
                activityprogress=p
            ).aggregate(total_progress = Sum('amount'))
            try:
                percent = int(suma['total_progress']/p.amount*100)
            except:
                percent = 0
            progress.append({
                'id':p.pk,
                'name':p.name,
                'total':suma['total_progress'],
                'goal':p.amount,
                'unit':p.unit,
                'percent': percent,
            })
        context['progress']=progress
        return context

class ActivityUpdateView(LoginRequiredMixin, UpdateView):
    model = Activity
    form_class = ActivityCreateForm
    success_url = reverse_lazy('projects:activity_list')

    def get_success_url(self) -> str:
        return reverse_lazy('projects:activity_detail', kwargs={'pk':self.kwargs['pk']})

class ActivityDeleteView(LoginRequiredMixin, DeleteView):
    model = Activity
    success_url = reverse_lazy('projects:activity_list')
    context_object_name = 'activity'

class RequirementListView(LoginRequiredMixin, ListView):
    model = Requirements
    context_object_name = 'requirements_list'
    paginate_by=20

    def get_queryset(self) -> QuerySet[Any]:
        raw_status = self.request.GET.get('status')
        if raw_status:
            status = int(raw_status)
            queryset = Requirements.objects.filter(status = status)
        elif self.request.GET.get('mios'):
            queryset = Requirements.objects.filter(
                to = self.request.user.pk
            )
        else:
            queryset = Requirements.objects.all()
        return queryset

class RequirementCreateView(LoginRequiredMixin, CreateView):
    model = Requirements
    form_class = RequirementCreateForm
    success_url = reverse_lazy('projects:requirement_list')

    def get_form_kwargs(self):
        kwargs = super(RequirementCreateView, self).get_form_kwargs()
        u = self.request.user
        kwargs['initial'] = {
            'by':u.pk,
            'to':7,
            'supply_date':datetime.datetime.now() + datetime.timedelta(days=2)
        }
        return kwargs
    
class RequirementUpdateView(LoginRequiredMixin, UpdateView):
    model = Requirements
    form_class = RequirementCreateForm

    def get_success_url(self) -> str:
        return '{}?status=0'.format(reverse_lazy('projects:requirement_list'))

class RequirementCompleteView(LoginRequiredMixin, RedirectView):

    def get_redirect_url(self, *args: Any, **kwargs: Any) -> str | None:
        request_to_complete = self.kwargs['pk']
        try:
            req = Requirements.objects.get(pk=request_to_complete)
            req.status = 1
            req.save()
        except:
            print('no se pudo acutalizar')
        url = '{}?status=0'.format(
            reverse_lazy('projects:requirement_list')
        )
        return url

class ReportCardListView(LoginRequiredMixin, ListView):
    model = Report
    context_object_name = 'report_list'
    paginate_by = 10
    ordering = '-date'
    template_name = 'projects/report_card_list.html'

class ReportListView(LoginRequiredMixin, ListView):
    model = Report
    context_object_name = 'report_list'
    paginate_by = 10
    #ordering = '-date'

    def get_queryset(self):
        # print(self.request.GET.get('mios'))
        if self.request.GET.get('mios'):
            u = self.request.user
            queryset = Report.objects.filter(responsable=u).order_by('-date')
        elif self.request.GET.get('noact'):
            queryset = Report.objects.filter(activity=None)
        elif self.request.GET.get('adicional'):
            queryset = Report.objects.filter(activity__type = 0, activity__status__lte = 1).order_by('activity')
        elif self.request.GET.get('oficina'):
            users = User.objects.filter(groups__name = 'Oficina')
            #print(users)
            queryset = Report.objects.filter(responsable__in=users).order_by('-date')
        else:
            queryset = Report.objects.all().order_by('-date')
        return queryset

class ReportCreateView(LoginRequiredMixin, CreateView):
    model = Report
    form_class = ReportCreateForm

    def get_form_kwargs(self):
        kwargs = super(ReportCreateView, self).get_form_kwargs()
        u = self.request.user
        kwargs['initial'] = {
            'responsable':u.pk,
        }
        activity = self.request.GET.get('activity')
        if activity:
            kwargs['initial']['activity']=activity
        return kwargs

    def get_success_url(self):
        try:
            if self.object.activity.activityprogress_set.all():
                return reverse_lazy('projects:report2_create', kwargs = {'pk':self.object.pk})
            return reverse_lazy('projects:report_detail', kwargs = {'pk':self.object.pk})
        except:
            return reverse_lazy('projects:report_detail', kwargs = {'pk':self.object.pk})

class ReportUpdateView(LoginRequiredMixin, UpdateView):
    model = Report
    form_class = ReportCreateForm
    success_url = reverse_lazy('projects:report_list')

    def get_success_url(self) -> str:
        return reverse_lazy('projects:report_detail', kwargs={'pk': self.kwargs['pk']})

class ReportDetailView(LoginRequiredMixin, DetailView):
    model = Report
    context_object_name = 'report'

class ReportDeleteView(LoginRequiredMixin, DeleteView):
    model = Report
    context_object_name = 'report'
    success_url = reverse_lazy('projects:report_list')

class PhotoCreateView(LoginRequiredMixin, FormView):
    form_class = PhotoForm
    template_name='projects/photo_form.html'

    def get_success_url(self):
        return self.request.GET.get('next') or reverse_lazy('projects:report_detail', kwargs={'pk':self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['report'] = Report.objects.get(pk=self.kwargs.get("pk"))
        context['photos'] = Photo.objects.filter(report = self.kwargs.get("pk"))
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['report'] = self.kwargs.get("pk")
        return initial
        
    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        files = form.cleaned_data["pictures"]
        report = Report.objects.get(pk=int(self.kwargs.get("pk")))
        THUMBNAIL_SIZE = (80, 80)
        PIL_TYPE = ''
        for f in files:
            p = Photo.objects.create(report = report, pictures=f)
            DJANGO_TYPE = f.content_type
            if DJANGO_TYPE == 'image/jpeg':
                PIL_TYPE = 'jpeg'
            elif DJANGO_TYPE == 'image/png':
                PIL_TYPE = 'png'
            if PIL_TYPE in ['jpeg', 'png']:
                with p.pictures.open() as original:
                    image = Image.open(original)
                    image.thumbnail(THUMBNAIL_SIZE)
                    image.save('temp', format = PIL_TYPE)
                    with open('temp', 'rb') as thum:
                        p.thumbnail.save(
                            original.name,
                            UploadedFile(
                                file = thum,
                                content_type=DJANGO_TYPE
                            )
                        )
        return super().form_valid(form)

class ReportDayArchiveView(LoginRequiredMixin, DayArchiveView):
    model = Report
    date_field = 'date'
    day_format = '%d'
    context_object_name = 'reports'
    allow_empty = True
    ordering = 'activity'

class ReportLastdayArchiveView(LoginRequiredMixin, RedirectView):
    year = datetime.date.today().year
    month = datetime.date.today().strftime('%b')
    day = datetime.date.today().day
    try:
        ultima_fecha = Report.objects.all().order_by('-date')[0].date
        #print(ultima_fecha)
    except:
        ultima_fecha = None
    queryset = Report.objects.filter(date = ultima_fecha).order_by('activity')
    url = reverse_lazy(
        'projects:report_dayarchive',
        kwargs = {
            'year':year,
            'month':month,
            'day':day,
        }
    )

class ActivityProgressCreateView(LoginRequiredMixin, CreateView):
    model = ActivityProgress
    form_class = ActivityprogressForm

    def get_initial(self):
        initial = super().get_initial()
        initial['activity'] = self.kwargs.get("pk")
        return initial

    def get_success_url(self):
        return self.request.GET.get('next') or reverse_lazy('projects:activity_detail', kwargs={'pk':self.kwargs['pk']})

class ActivityProgressUpdateView(LoginRequiredMixin, UpdateView):
    model = ActivityProgress
    form_class = ActivityprogressForm

    def get_success_url(self):
        return reverse_lazy('projects:activity_detail', kwargs={'pk':self.object.activity.pk})

class ActivityProgressDeleteView(LoginRequiredMixin, DeleteView):
    model = ActivityProgress
    context_object_name = 'activityprogress'

    def get_success_url(self):
        return reverse_lazy('projects:activity_detail', kwargs={'pk':self.object.activity.pk})

class ReportProgressCreateView(LoginRequiredMixin, FormView):
    form_class = ReporprogressForm
    template_name = 'projects/report2_form.html'
    success_url = reverse_lazy("projects:report_list")

    def get_success_url(self) -> str:
        return reverse_lazy("projects:report_detail", kwargs={'pk':self.kwargs['pk']})

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super(ReportProgressCreateView, self).get_form_kwargs()
        kwargs['initial'] = {
            'act':Report.objects.get(
                pk = self.kwargs['pk'],
            ).activity.pk
            }
        return kwargs

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
        
    def form_valid(self, form):
        return super().form_valid(form)
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        return context
    
    def form_valid(self, form):
        progress_fields = ActivityProgress.objects.filter(
            activity = Report.objects.get(
                pk = self.kwargs['pk']
            ).activity
        )
        for progress_field in progress_fields:
            if(form.data[progress_field.name]):
                #print('val: {}'.format(form.data[progress_field.name]))
                r = ReportProgress.objects.create(
                    activityprogress = progress_field,
                    report = Report.objects.get(pk = self.kwargs['pk']),
                    amount = float(form.data[progress_field.name])
                )
        return super().form_valid(form)

class ReportProgressUpdateView(LoginRequiredMixin, UpdateView):
    model = ReportProgress
    fields = ['amount']
    template_name = 'projects/report2_form.html'

    def get_success_url(self):
        return reverse_lazy('projects:report_detail', kwargs={'pk':self.object.report.pk})
    
class ReportProgressDeleteView(LoginRequiredMixin, DeleteView):
    model = ReportProgress
    context_object_name = 'reportprogress'


    def get_success_url(self):
        #print(self.object.report.pk)
        return reverse_lazy('projects:report_detail', kwargs={'pk':self.object.report.pk})
    
class SearchListView(LoginRequiredMixin, TemplateView):
    template_name = 'search_results.html'

    def post(self, request, *args, **kwargs):
        searched = request.POST['valuesearch']
        context = {}
        context['searched'] =  searched
        context['projects'] = Project.objects.filter(name__icontains = searched)
        context['activities'] = Activity.objects.filter(name__icontains = searched)
        context['reports'] = Report.objects.filter(description__icontains = searched)
        return render(
            template_name='search_results.html',
            request=request,
            context=context
        )