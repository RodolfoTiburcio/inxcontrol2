from django import forms
from django.db.models import Q

from django.contrib.auth.models import User
from django.forms import ModelChoiceField, ModelMultipleChoiceField

from .models import (Project, Activity, Requirements, Report, Photo,
                     ActivityProgress, ReportProgress)

class UserModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.get_full_name()

class UserModelMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.get_full_name()

class ProjectCreateForm(forms.ModelForm):
    manager = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
    )
    class Meta:
        model = Project
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'date':'Fecha inicio',
            'type':'Tipo',
            'end_date':'Fecha final',
            'client':'Cliente',
            'contact':'Contacto',
            'manager':'Administrador',
            'status':'Estado',
        }
        widgets = {
            'date':forms.DateInput(attrs={
                'class':'datepicker form_control',
                'type':'date',
            }),
            'end_date':forms.DateInput(attrs={
                'class':'datepicker form_control',
                'type':'date',
            })
        }

class ActivityCreateForm(forms.ModelForm):
    incharge = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
        label = 'Responsable',
    )
    project = forms.ModelChoiceField(
        queryset= Project.objects.filter(status=0).order_by('-date'),
        label = 'Proyecto',
        empty_label="(Sin proyecto)",
        required=False,
    )
    class Meta:
        model = Activity
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'date':'Fecha de inicio',
            'end_date':'Fecha final',
            'description':'Descripción',
            'type':'Tipo',
            'status':'Estado',
        }
        widgets = {
            'date':forms.DateInput(attrs={
                'class':'datepicker form_control',
                'type':'date',
            }),
            'end_date':forms.DateInput(attrs={
                'class':'datepicker form_control',
                'type':'date',
            }),
            'type':forms.RadioSelect(),
            'status':forms.RadioSelect(),
        }

class RequirementCreateForm(forms.ModelForm):
    by = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
        label = 'Solicitante',
    )
    to = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
        label = 'Responsable',
    )
    class Meta:
        model = Requirements
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'request_date':'Solicitado el',
            'supply_date':'Para entregar el',
            'description': 'Descripción',
            'amount':'Cantidad',
            'unit':'Unidad',
            'activity':'Actividad',
            'status':'Estado',
        }
        widgets = {
            'request_date':forms.DateInput(attrs={
                'class':'datepicker form_control',
                'type':'date',
            }),
            'supply_date':forms.DateInput(attrs={
                'class':'datepicker form_control',
                'type':'date',
            }),
            'status':forms.RadioSelect(),
        }

class ReportCreateForm(forms.ModelForm):

    responsable = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
    )
    workers = UserModelMultipleChoiceField(
        queryset=User.objects.filter(is_active=True),
        widget=forms.CheckboxSelectMultiple,
    )
    activity = forms.ModelChoiceField(
        queryset=Activity.objects.filter(status__lte=1).order_by('project'),
        required=False,
        # queryset=Activity.objects.filter(
        #     Q(status__lte=1) & Q(type=4)
        # ),
    )
    class Meta:
        model = Report
        fields = '__all__'
        labels = {
            'responsable':'Responsable',
            'date':'Fecha',
            'name':'Nombre',
            'activity':'Actividad',
            'description':'Descripción',
            'workers':'Trabajadores',
            'duration':'Duración',
            'deadtime':'Tiempo muerto',
            'deadtimereason':'Tipo de tiempo muerto',
            'extratime':'Tiempo extra'
        }
        widgets = {
            'date':forms.DateInput(attrs={
                'class':'datepicker form_control',
                'type':'date',
            }),
            'deadtimereason':forms.RadioSelect(),
            'extratime':forms.CheckboxInput(),
        }

class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True

class MultipleFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        super().__init__(*args, **kwargs)
    
    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = single_file_clean(data, initial)
        return result

class PhotoForm(forms.Form):
    pictures = MultipleFileField()
    class Meta:
        model = Photo
        fields = '__all__'
        widgets = {
            'report' : forms.HiddenInput(),#forms.Select(attrs={'class':'form-control'}),
        }

class ActivityprogressForm(forms.ModelForm):
    class Meta:
        model = ActivityProgress
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'amount':'Cantidad',
            'unit':'Unidad',
        }
        widgets = {
            'activity':forms.HiddenInput(),
        }

class ReporprogressForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        progress = ActivityProgress.objects.filter(
            activity = self.initial['act']
        )
        for each in progress:
            field_name = each.name
            self.fields[field_name]=forms.DecimalField(
                max_digits=6,
                decimal_places=2,
                required=False,
                help_text = 'Avance en {unit} de un total de {amount} {unit}'.format(
                    unit = each.unit,
                    amount = each.amount
                ),
                initial=0.0,
            )